﻿using System.Drawing;
using static WindowsFormsApplication3.Form1;

namespace WindowsFormsApplication3
{
    /// <summary>  
    ///  Класс для создания и отображения на форме окружностей
    /// </summary>
    public class Circle : Figure
    {
        public System.Drawing.Rectangle rectangle;

        public Circle(Point[] points, float width) : base(points, width) { }

        public override void DrawFigure(Graphics graph, Pen pen)
        {
            graph.DrawEllipse(pen, points[0].X, points[0].Y, width, width);
        }
    }
}
