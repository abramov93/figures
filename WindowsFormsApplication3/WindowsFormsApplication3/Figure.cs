﻿using System.Drawing;

namespace WindowsFormsApplication3
{
    /// <summary>  
    ///  Класс для создания и отображения на форме геометрических фигур
    /// </summary>
    public class Figure
    {
        public const float PI = 3.14159f;
        
        /// <summary>
        /// Ширина фигуры
        /// </summary>
        public float width;
        
        /// <summary>
        /// Массив вершин для фигур
        /// </summary>
        public Point[] points; 

        public Figure(Point[] points, float width)
        {
            this.width = width;
            this.points = points;
        }

        /// <summary>
        /// Метод отображения фигур на форме
        /// </summary>
        /// <param name="bmp"></param>
        public void Draw(Bitmap bmp)
        {
            Graphics graph = Graphics.FromImage(bmp);
            Pen pen = new Pen(Color.Blue);
            DrawFigure(graph, pen);
        }

        /// <summary>
        /// Вспомогательный переопределяемый метод для метода Draw()
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="pen"></param>
        public virtual void DrawFigure(Graphics graph, Pen pen) { }
    }
}
