﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Объект r позволяет задать случайную величину
        /// </summary>
        public static Random r;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            r = new Random();

            Bitmap bmp = new Bitmap(picture.Width, picture.Height);

            Point[] pointMass_1, pointMass_3, pointMass_4, pointMass_6, pointMass_16;

            List<Figure> figureMass;

            // Создание массивов вершин для многоугольников
            pointMass_1 = CreatePointMass(1);
            pointMass_3 = CreatePointMass(3);
            pointMass_4 = CreatePointMass(4);
            pointMass_6 = CreatePointMass(6);
            pointMass_16 = CreatePointMass(16);

            // СОздание коллекции объектов фигур
            figureMass = new List<Figure>();
            figureMass.Add(new Rectangle(pointMass_3));
            figureMass.Add(new Rectangle(pointMass_4));
            figureMass.Add(new Rectangle(pointMass_6));
            figureMass.Add(new Rectangle(pointMass_16));
            figureMass.Add(new Circle(pointMass_1, r.Next(50, 120)));

            // Отображение созданной коллекции фигур на плоскости
            ShowFigres(figureMass, bmp);

            picture.Image = bmp;
        }

        /// <summary>
        /// Метод для создания коллекции вершин фигуры
        /// </summary>
        /// <param name="angleNmber"></param>
        public Point[] CreatePointMass(int angleNumber)
        {
            Point[] pointMass = new Point[angleNumber];

            for (int i = 0; i < angleNumber; i++)
            {
                switch (angleNumber)
                {
                    case 1: pointMass[i] = new Point(r.Next(470, 480), r.Next(40, 70)); break;
                    case 3: pointMass[i] = new Point(r.Next(10, 100), r.Next(50, 100)); break;
                    case 4: pointMass[i] = new Point(r.Next(110, 200), r.Next(50, 100)); break;
                    case 6: pointMass[i] = new Point(r.Next(210, 300), r.Next(50, 100)); break;
                    case 16: pointMass[i] = new Point(r.Next(310, 400), r.Next(50, 100)); break;
                }
            }
            return pointMass;
        }

        /// <summary>
        /// Отображение созданных объектов коллекции
        /// </summary>
        /// <param name="figureMass"></param>
        /// <param name="bmp"></param>
        private void ShowFigres(List<Figure> figureMass, Bitmap bmp)
        {       
            for (int i = 0; i < figureMass.Count; i++)
                figureMass[i].Draw(bmp);
        }
    }
}
