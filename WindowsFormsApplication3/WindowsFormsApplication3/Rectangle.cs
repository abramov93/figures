﻿using System.Drawing;
using static WindowsFormsApplication3.Form1;

namespace WindowsFormsApplication3
{
    /// <summary>  
    ///  Класс для создания и отображения на форме многоугольников
    /// </summary>
    public class Rectangle : Figure
    {
        public Rectangle(Point[] points) : this(points, 0) { }

        Rectangle(Point[] points, float width) : base(points, width) { }

        public override void DrawFigure(Graphics graph, Pen pen)
        {
            graph.DrawPolygon(pen, points);
        }
    }
}
